#!/bin/bash
file=${WORKSPACE}/group_vars/all.yml

list_of_envs=(`env | cut -d'=' -f1`)

for env in "${list_of_envs[@]}"
do

  key="$( echo ${env} )"
  value="$( echo ${!env} )"
  if [[ -n "$value" ]]
  then
    #echo "Replacing parameter $key with $value"
    sed -i.bkp s#"^\($key:\).*$"#"\1 $value"#g $file
    shift
  fi


done
lang=`echo $platform`
ver=`echo $version`
postfix="_lib_version"
echo "Replacing parameter $lang$postfix with $ver"
if [[ -n "$ver" ]]
then
 sed -i.bkp s#"^\($lang$postfix:\).*$"#"\1 $ver"#g $file
fi
