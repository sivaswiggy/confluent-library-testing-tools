package com.swiggy.kafka.client.consumer;

import com.swiggy.kafka.client.Application;
import com.swiggy.kafka.client.common.Env;
import com.swiggy.kafka.client.common.Helpers;
import com.swiggy.kafka.client.pojo.Message;
import com.swiggy.kafka.clients.configs.CommonConfig;
import com.swiggy.kafka.clients.configs.ConsumerConfig;
import com.swiggy.kafka.clients.configs.ProducerConfig;
import com.swiggy.kafka.clients.configs.Topic;
import com.swiggy.kafka.clients.consumer.Consumer;
import com.swiggy.kafka.clients.consumer.Handler;
import com.swiggy.kafka.clients.producer.Producer;

import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SampleConsumer {

    public static AtomicLong processedCount = new AtomicLong(0);
    public static AtomicLong duplicateCount = new AtomicLong(0);
    public static Logger logger = Logger.getLogger(SampleConsumer.class.getName());
    public static boolean isProcessed = false;
    public static Message lastProcessedMessage = null;


    public static void loadConsumerDefaults() {
        Application.argumentMap.put(ConsumerEnv.CONCURRENCY,"1");
        Application.argumentMap.put(ConsumerEnv.CONSUMER_CLIENT_ID,"algates_consumer");
        Application.argumentMap.put(ConsumerEnv.CONSUMER_GROUP_ID,"algates_consumer_group_0");
        Application.argumentMap.put(ConsumerEnv.MAX_POLL_RECORDS,"500");
        Application.argumentMap.put(ConsumerEnv.IS_LATEST_OFFSET,"true");
        Application.argumentMap.put(ConsumerEnv.IS_INTERCEPTOR_ENABLED,"true");
        Application.argumentMap.put(ConsumerEnv.MESSAGE_PROCESSING_TIME,"0");

    }

    public static void consume() {

        if (Application.argumentMap.get(Env.LOG_LEVEL).equals("ALL")){
            logger.setLevel(Level.ALL);
        } else {
            logger.setLevel(Level.INFO);
        }

        Topic topic = Helpers.buildTopic();

        CommonConfig.Cluster primaryCluster = Helpers.buildPrimaryCluster();
        CommonConfig.Cluster secondaryCluster = Helpers.buildSecondaryCluster();

        ConsumerConfig consumerConfig = Helpers.buildConsumerConfig(topic,primaryCluster,secondaryCluster);
        Handler handler = new MessageHandler();
        Consumer consumer = null;
        if (Boolean.parseBoolean(Application.argumentMap.get(Env.FAULT_STRATEGY))) {
            ProducerConfig producerConfigs = Helpers.buildProducerConfig(topic,primaryCluster,secondaryCluster);
            Producer retryProducer = new Producer(producerConfigs);
            consumer = new Consumer(consumerConfig, handler, retryProducer);
        } else {
            consumer = new Consumer(consumerConfig, handler);
        }
        consumer.start();
        logger.info("Consumer started");

        long now = System.currentTimeMillis();
        long startOfTheRun = now;
        long end = now + System.currentTimeMillis();
        int runDuration = Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION));

        if ((Integer.parseInt(Application.argumentMap.get(Env.MAX_MESSAGES_TO_BE_PROCESSED))>0) && (runDuration==0)) {
            end = Long.MAX_VALUE;
        } else if ((Integer.parseInt(Application.argumentMap.get(Env.MAX_MESSAGES_TO_BE_PROCESSED))==0) && (runDuration>0)) {
            end = now + (Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION)) * 1000);
        }

        while (end>now) {
            if (isProcessed) {
                now = end;
            } else if (runDuration > 0) {
                now = System.currentTimeMillis();
            }
        }

        long endOfTheRun = System.currentTimeMillis();
        long  testDuration = (endOfTheRun - startOfTheRun) / 1000;

        if (lastProcessedMessage != null ) {
            logger.info("last processed message timestamp ---> " + lastProcessedMessage.getTimeStamp());
            logger.info("Total messages consumed / sec --->" + "" + processedCount.get() / (Integer.parseInt(Application.argumentMap.get(Env.RUN_DURATION))));
            logger.info("Total messages consumed for " + testDuration +" secs --->" + "" + processedCount.get());
        } else {
            logger.info("Total messages consumed for " + testDuration + " secs---> 0");
        }

        if (Boolean.parseBoolean(Application.argumentMap.get(Env.IS_REDIS_ENABLED))) {
            logger.info("Total no of duplicates --->" + "" + duplicateCount.get());
        } else {
            logger.info("For getting duplicate count enable redis and pass redis host and port using --isRedisEnabled --redisHost --redisPort");
        }

        logger.info("Consumer done");
        System.exit(0);

    }

}
