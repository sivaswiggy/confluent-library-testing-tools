package com.swiggy.kafka.client.consumer;

public class ConsumerEnv {

    public static final String IS_LATEST_OFFSET = "--isLatestOffset";
    public static final String CONSUMER_CLIENT_ID = "--consumerClientId";
    public static final String CONSUMER_GROUP_ID = "--consumerGroupId";
    public static final String MAX_POLL_RECORDS = "--maxPollRecords";
    public static final String CONCURRENCY = "--concurrency";
    public static final String IS_INTERCEPTOR_ENABLED = "--replicateOffset";
    public static final String MESSAGE_PROCESSING_TIME = "--processingTime";


}
