package com.swiggy.kafka.client.producer;

import com.swiggy.kafka.client.common.Env;

public class ProducerEnv extends Env {

    public static final String NO_OF_THREADS = "--threads";
    public static final String NO_OF_RETRIES = "--retries";
    public static final String ACK = "--ack"; //ALL ONE NONE
    public static final String IS_SYNC = "--isSync";
    public static final String IS_CALLBACKS = "--isCallbackEnabled";
    public static final String IS_COMPRESSED = "--isCompressed";
    public static final String MESSAGE_FILE_PATH = "--messageFilePath";
    public static final String PRODUCER_CLIENT_ID = "--producerClientId";
    public static final String MESSAGES_PER_SECOND = "--noOfMessagesPerSecond";
    public static final String EXPECTED_PRODUCED_MESSAGECOUNT = "--expectedMessageCount";
    public static final String TOLERANCE_PERCENTAGE = "--tolerancepercentage";

}
